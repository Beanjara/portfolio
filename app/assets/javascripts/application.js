// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
// require turbolinks
//
//
//= require fontawesome/js/all
//= require plugins/jquery-3.4.1.min
//= require plugins/popper.min    
//= require plugins/jquery-ui
//= require plugins/bootstrap/js/bootstrap.min
//= require plugins/back-to-top
//= require plugins/lightbox/ekko-lightbox.min
//= require plugins/flickity/flickity.pkgd.min
//= require plugins/imagesloaded.pkgd.min
//= require plugins/isotope.pkgd.min
//= require main
//= require isotope-custom
//= require lightbox-custom
//= require flickity-custom
//
//
//= require_tree .
