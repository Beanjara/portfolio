module ApplicationHelper
    
    def title_for_page(page_title)
        base_title = "Be Anjara's Portfolio"
        if page_title.empty?
            page_title = base_title
        else
            page_title = page_title
        end
        # page_title.empty? ?  page_title : "#{base_title}"
    end
end
