require 'test_helper'

class ProjectControllerTest < ActionDispatch::IntegrationTest
  test "should get hoomies" do
    get project_hoomies_url
    assert_response :success
  end

  test "should get vegefoods" do
    get project_vegefoods_url
    assert_response :success
  end

  test "should get catstore" do
    get project_catstore_url
    assert_response :success
  end

end
