Rails.application.routes.draw do
  get 'contact/new'
  get 'contact/create'
  get 'resume/index'
  get 'project/hoomies'
  get 'project/vegefoods'
  get 'project/catstore'
  get 'contact/index'
  get 'project/index'
  get 'resume/download'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root :to => "home#index"
end
